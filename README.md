Reduced Order Modeling library of the FUI project "MOR_DICUS".


# 1) INSTALLATION

The dependencies of Mordicus are:

* for user:
  * python>=3.9 (tested 3.9 and 3.11)
  * numpy>=1.22.1
  * scikit-learn
  * mpi4py
* for developers, running tests and building the documentation, add:
  * setuptools>=61.0.0
  * pip
  * pytest
  * pytest-cov
  * sphinx
  * sphinx-rtd-theme

Users can install the anaconda package with

    conda install -c drti mordicus

Alternatively, and for developers, the repository can be downloaded with

    git clone https://gitlab.com/mor_dicus/mordicus.git

Then, once the required dependencies have been installed, path/to/mordicus/src should be added to the PYTHONPATH environment variable.

A conda package can be also created using the recipe available in the repository in

    path/to/mordicus/recipes/noarch/

### Remark

Warning on HPC architectures: the use of mpi4py may require an install from pip, after loading a mpi installation adapted to the cluster. If using together with other loaded module, be mindful of which version of mpirun you are using.

### Remark

Windows is partially supported:
[MS-MPI](https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi#ms-mpi-downloads)
should be installed. Both msmpisetup.exe (MS-MPI binaries) and msmpisdk.msi (sdk, required for mpi4py).


# 2) TESTING

There are two different type of tests: unit tests and simplified use cases, both checking non-regression by compararing the produced values with precomputed references. Both can be executed either individually by browsing in the corresponding folders and executing each python script, or globally using pytest.


* ## Unit testing

  Checks all functions and produces a coverage report.

      cd path/to/mordicus
      pytest tests --cov=src/Mordicus --cov-report=html:path/to/build/html


  ### Remark

    On Windows, noncompatible tests must be bypassed:

      cd path/to/mordicus
      pytest -m "not incompatiblewindows" tests --cov=src/Mordicus --cov-report=html:path/to/build/html


* ## Simplified use cases

  Illustrates the functionalities of the library in complete use cases.

      cd path/to/mordicus
      pytest examples


# 3) DOCUMENTATION


A sphinx documentation can be generated from the main folder:

    cd path/to/mordicus
    python setup.py build_sphinx

The documentation is also available on [readthedocs](https://mordicus.readthedocs.io/).

# 4) CONTRIBUTION AND LICENSE

See the CONTRIBUTING.md and LICENSE files.
