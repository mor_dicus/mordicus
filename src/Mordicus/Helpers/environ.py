# coding: utf-8
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#
import os

def boolenv(name, default=False):
    """Return the value True or False of a shell variable supposed to
    be a boolean value (0 or 1, etc.)"""
    if name not in os.environ: return default
    b = os.environ[name]
    if b.lower() in ['1','y','o','yes','oui','true']:
        return True
    return False

# Specify hereafter the global parameters of MORDICUS that can be
# initialized from shell variable

# If true, then the user message printed using the log function are
# printed on the standard output, otherwize they are not. Used for
# example for ExternalSolvingProcedure 
MDX_LOG_DISPLAY = boolenv("MDX_LOG_DISPLAY", True)                   
