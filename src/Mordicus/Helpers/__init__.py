# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

"""
Helpers contain simple functions aiming to provide basic services, unrelated
to model order reduction.
"""