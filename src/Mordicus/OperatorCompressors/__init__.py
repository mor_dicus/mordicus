# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

"""
OperatorCompressors are functions enabling efficient construction of
reduced problems.
"""