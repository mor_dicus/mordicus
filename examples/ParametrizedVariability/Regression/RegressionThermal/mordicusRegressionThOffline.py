# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from Mordicus.DataCompressors import SnapshotPOD
from Mordicus.OperatorCompressors import Regression
from Mordicus.IO import StateIO as SIO
from Mordicus.Helpers import FolderHandler as FH
import numpy as np



def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()

    sol = SIO.LoadState("sol")

    solutionName = "TP"
    nbeOfComponents = 1
    numberOfNodes = 676
    primality = True

    collectionProblemData = CPD.CollectionProblemData()

    collectionProblemData.AddVariabilityAxis('Text', float, quantities=('temperature', 'K'), description="this is Text")
    collectionProblemData.AddVariabilityAxis('Tint', float, quantities=('temperature', 'K'), description="this is Tint")

    collectionProblemData.DefineQuantity(solutionName, "Temperature", "Celsius")

    parameters = [[100.0, 1000.0], [90.0, 1100.0], [110.0, 900.0], [105.0, 1050.0]]

    for i in range(4):

        problemData = PD.ProblemData("myProblem_"+str(i))

        outputTimeSequence = []

        solution = S.Solution(
            solutionName=solutionName,
            nbeOfComponents=nbeOfComponents,
            numberOfNodes=numberOfNodes,
            primality=primality,
        )

        problemData.AddSolution(solution)

        for time, snapshot in sol[i].items():
            solution.AddSnapshot(snapshot, time)
            problemData.AddParameter(np.array(parameters[i] + [time]), time)
            outputTimeSequence.append(time)

        collectionProblemData.AddProblemData(problemData, Text=parameters[i][0], Tint=parameters[i][1])


    print("Solutions have been read")

    ##################################################
    # OFFLINE
    ##################################################

    reducedOrderBasis = SnapshotPOD.ComputeReducedOrderBasisFromCollectionProblemData(
        collectionProblemData, solutionName, 1.e-2
    )
    collectionProblemData.AddReducedOrderBasis(solutionName, reducedOrderBasis)
    print("A reduced order basis has been computed has been constructed using SnapshotPOD")

    collectionProblemData.CompressSolutions("TP")
    print("The solution has been compressed")


    from sklearn.gaussian_process.kernels import WhiteKernel, ConstantKernel, Matern
    from sklearn.gaussian_process import GaussianProcessRegressor

    kernel = ConstantKernel(constant_value=1.0, constant_value_bounds=(0.01, 10.0)) * Matern(length_scale=1., nu=2.5) + WhiteKernel(noise_level=1, noise_level_bounds=(1e-10, 1e0))

    regressors = {"TP":GaussianProcessRegressor(kernel=kernel)}

    paramGrids = {}
    paramGrids["TP"] = {'kernel__k1__k1__constant_value':[0.1, 1.], 'kernel__k1__k2__length_scale': [1., 3., 10.], 'kernel__k2__noise_level': [1., 2.]}

    Regression.CompressOperator(collectionProblemData, regressors, paramGrids)

    SIO.SaveState("collectionProblemData", collectionProblemData)


    ##################################################
    ## check accuracy compression
    ##################################################

    compressionErrors = []

    for _, problemData in collectionProblemData.GetProblemDatas().items():
        solution = problemData.GetSolution("TP")
        CompressedSolution = solution.GetReducedCoordinates()
        for t in solution.GetTimeSequenceFromReducedCoordinates():
            reconstructedReducedCoordinates = np.dot(CompressedSolution[t], reducedOrderBasis)
            exactSolution = solution.GetSnapshotAtTime(t)
            norml2ExactSolution = np.linalg.norm(exactSolution)
            if norml2ExactSolution != 0:
                relError = np.linalg.norm(reconstructedReducedCoordinates-exactSolution)/norml2ExactSolution
            else:
                relError = np.linalg.norm(reconstructedReducedCoordinates-exactSolution)
            compressionErrors.append(relError)

    print("compressionErrors =", compressionErrors)

    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-2, "!!! Regression detected !!! compressionErrors have become too large"



if __name__ == "__main__":
    test()
