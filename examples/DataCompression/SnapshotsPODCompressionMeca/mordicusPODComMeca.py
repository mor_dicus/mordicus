# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.Containers import ProblemData as PD
from Mordicus.Containers import CollectionProblemData as CPD
from Mordicus.Containers import Solution as S
from Mordicus.DataCompressors import SnapshotPOD
from Mordicus.Helpers import FolderHandler as FH
from Mordicus.IO import StateIO as SIO
import numpy as np


def test():


    folderHandler = FH.FolderHandler(__file__)
    folderHandler.SwitchToScriptFolder()

    sol = SIO.LoadState("sol")


    collectionProblemData = CPD.CollectionProblemData()
    collectionProblemData.DefineVariabilityAxes(('config',), (str, ))
    collectionProblemData.DefineQuantity("U", "displacement", "m")

    problemData = PD.ProblemData("myProblem")


    nbeOfComponents   = 3
    meshNumberOfNodes = 343

    solution = S.Solution("U", nbeOfComponents, meshNumberOfNodes, primality = True)

    outputTimeSequence = []
    for time, snapshot in sol.items():
        solution.AddSnapshot(snapshot, time)
        outputTimeSequence.append(time)

    problemData.AddSolution(solution)

    collectionProblemData.AddProblemData(problemData, config="case-1")
    print(
        "A collectionProblemData with problemDatas "
        + str(collectionProblemData.GetProblemSampling())
        + " has been constructed"
    )


    ##################################################

    reducedOrderBasis = SnapshotPOD.ComputeReducedOrderBasisFromCollectionProblemData(
        collectionProblemData, "U", 1.e-8
    )
    collectionProblemData.AddReducedOrderBasis("U", reducedOrderBasis)
    print("A reduced order basis has been computed has been constructed using SnapshotPOD")

    collectionProblemData.CompressSolutions("U")
    print("The solution has been compressed")

    compressedSolution = solution.GetReducedCoordinates()

    ##################################################
    ## check accuracy compression
    ##################################################


    compressionErrors = []

    for t in outputTimeSequence:

        reconstructedCompressedSolution = np.dot(compressedSolution[t], reducedOrderBasis)
        exactSolution = solution.GetSnapshot(t)
        norml2ExactSolution = np.linalg.norm(exactSolution)
        if norml2ExactSolution != 0:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)/norml2ExactSolution
        else:
            relError = np.linalg.norm(reconstructedCompressedSolution-exactSolution)
        compressionErrors.append(relError)

    print("compressionErrors =", compressionErrors)

    folderHandler.SwitchToExecutionFolder()

    assert np.max(compressionErrors) < 1.e-6, "!!! Regression detected !!! compressionErrors have become too large"


if __name__ == "__main__":
    test()
