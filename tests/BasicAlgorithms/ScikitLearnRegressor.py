# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

import numpy as np
from Mordicus.BasicAlgorithms import ScikitLearnRegressor as SLR


def test():


    from sklearn.gaussian_process.kernels import RBF

    kernel = RBF(length_scale_bounds=(1e-2, 1e2))

    regressor = SLR.MyGPR(kernel=kernel)

    paramGrid = {'kernel__length_scale': [1.]}

    one2 = np.ones(2)
    one4 = np.ones(4)

    X = np.array([one2, 2.*one2, 3.*one2, 7.*one2, one2, 2.*one2, 3.*one2, 9.*one2])
    y = np.array([one4, 2.*one4, 3.*one4, 7.*one4, one4, 2.*one4, 3.*one4, 9.*one4])

    model, scalerX, scalery = SLR.GridSearchCVRegression(regressor, paramGrid, X, y)
    yPred = SLR.ComputeRegressionApproximation(model, scalerX, scalery, X[:2])

    yPredRef = np.array(y[:2])

    np.testing.assert_almost_equal(yPred, yPredRef)


if __name__ == "__main__":
    print(test())  # pragma: no cover
