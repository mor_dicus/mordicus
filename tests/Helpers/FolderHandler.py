# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#

from Mordicus.Helpers import FolderHandler as FH
from Mordicus import GetTestPath
import os


def test():


    folderHandler = FH.FolderHandler(GetTestPath()+'Containers'+os.sep+'CollectionProblemData.py')

    folderHandler.SwitchToScriptFolder()
    assert folderHandler.scriptFolder == os.getcwd()

    folderHandler.SwitchToExecutionFolder()
    assert folderHandler.executionFolder == os.getcwd()


if __name__ == "__main__":
    print(test())  # pragma: no cover
