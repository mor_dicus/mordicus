# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.InitialConditions import InitialConditionBase as ICB


def test():

    initialCondition = ICB.InitialConditionBase()

    print(initialCondition)


if __name__ == "__main__":
    print(test())  # pragma: no cover
