# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.FieldHandlers import FieldHandlerBase as FHB


def test():


    fieldHandler = FHB.FieldHandlerBase()

    fieldHandler.SetInternalStorage("toto")
    assert fieldHandler.GetInternalStorage() == "toto"

    print(fieldHandler)


if __name__ == "__main__":
    print(test())  # pragma: no cover
