# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.Containers.OperatorCompressionData import OperatorCompressionDataRegression as OCDR
from Mordicus.BasicAlgorithms import  ScikitLearnRegressor as SLR
import numpy as np


def test():


    from sklearn.gaussian_process.kernels import RBF

    kernel = RBF(length_scale_bounds=(1e-2, 1e2))

    regressor = SLR.MyGPR(kernel=kernel)

    paramGrid = {'kernel__length_scale': [1.]}

    one2 = np.ones(2)
    one4 = np.ones(4)

    coefficients = np.array([one2, 2.*one2, 3.*one2, 7.*one2, one2, 2.*one2, 3.*one2, 9.*one2])
    parameters = np.array([one4, 2.*one4, 3.*one4, 7.*one4, one4, 2.*one4, 3.*one4, 9.*one4])


    model, scalerParameters, scalerCoefficients = SLR.GridSearchCVRegression(regressor, paramGrid, parameters, coefficients)

    operatorCompressionDataRegression = OCDR.OperatorCompressionDataRegression("U")

    operatorCompressionDataRegression.SetModel(model)
    operatorCompressionDataRegression.SetScalerParameters(scalerParameters)
    operatorCompressionDataRegression.SetScalerCoefficients(scalerCoefficients)


    assert id(operatorCompressionDataRegression.GetModel()) == id(model)
    assert id(operatorCompressionDataRegression.GetScalerParameters()) == id(scalerParameters)
    assert id(operatorCompressionDataRegression.GetScalerCoefficients()) == id(scalerCoefficients)

    print(operatorCompressionDataRegression)


if __name__ == "__main__":
    print(test())  # pragma: no cover


