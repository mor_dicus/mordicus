# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from Mordicus import GetTestDataPath, GetTestPath

def test():

    print("tests path is :", GetTestPath())
    print("TestsData path is :", GetTestDataPath())


if __name__ == "__main__":
    print(test())  # pragma: no cover
