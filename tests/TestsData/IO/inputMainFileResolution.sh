#!/bin/bash
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

# Testing substitution with parameters from input_mordicus_data
echo "Generating data from inputInstructionFile\n"

# Testing substitution with parameters for the dataset
python $(dirname ${BASH_SOURCE[0]})/inputInstructionFileResolution.py

mv snapshot.npy $(dirname ${BASH_SOURCE[0]})/snapshot.npy
