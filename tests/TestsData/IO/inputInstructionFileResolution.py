# coding: utf-8
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np


def run():

    mu1 = 0.0
    mu2 = 0.0

    a = np.linspace(mu1, mu2+19., 20)
    np.save("snapshot.npy", a, allow_pickle=False)


if __name__ == "__main__":
    run()