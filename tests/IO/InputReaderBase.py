# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#


from Mordicus.IO import InputReaderBase as IRB


def test():

    inputReaderBase = IRB.InputReaderBase()
    print(inputReaderBase)


if __name__ == "__main__":
    print(test())  # pragma: no cover
