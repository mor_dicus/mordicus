# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from setuptools import setup
from glob import glob
from pathlib import Path

fileList = glob('tests/**/*.*', recursive=True) + glob('examples/**/*.*', recursive=True) + ['tests/TestsData/IO/inputInstructionFile']
FolderList = [str(Path(file).parents[0]) for file in fileList]

setup(
    data_files=[('ressources/Mordicus/'+folder, [file]) for (folder,file) in zip(FolderList,fileList)]
)
