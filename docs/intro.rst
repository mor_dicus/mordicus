Installation
============

The dependencies of Mordicus are:

* for users:

  * python=3.9 (tested with 3.9 and 3.11)
  * numpy>=1.22.1
  * scikit-learn
  * mpi4py

* for developers, running tests and building the documentation, add:

  * setuptools>=61.0.0
  * pip
  * pytest
  * pytest-cov
  * sphinx
  * sphinx-rtd-theme

Users can install the conda package with

.. code-block:: bash

    conda install -c drti mordicus


Alternatively, and for developers, the repository can be downloaded with

.. code-block:: bash

    git clone https://gitlab.com/mor_dicus/mordicus.git

Then, once the required dependencies have been installed, path/to/mordicus/src should be added to the PYTHONPATH environment variable.

A conda package can be also created using the recipe available in the repository in

.. code-block:: bash

    path/to/mordicus/recipes/noarch/

.. warning::
    Warning on HPC architectures: the use of mpi4py may require an install from pip, after loading a mpi installation adapted to the cluster. If using together with other loaded module, be mindful of which version of mpirun you are using.

.. warning::
    Windows is partially supported:
    `MS-MPI <https://docs.microsoft.com/en-us/message-passing-interface/microsoft-mpi#ms-mpi-downloads>`_
    should be installed. Both msmpisetup.exe (MS-MPI binaries) and msmpisdk.msi (sdk, required for mpi4py).


Testing
=======

There are two different type of tests: unit tests and simplified use cases, both checking non-regression by compararing the produced values with precomputed references. Both can be executed either individually by browsing in the corresponding folders and executing each python script, or globally using pytest.


Unit testing
------------

Checks all functions and produces a coverage report.

.. code-block:: bash

    cd path/to/mordicus/tests
    pytest --cov=../src/Mordicus --cov-report=html:path/to/bluid/html


.. warning::
    On Windows, noncompatible tests must be bypassed:

    .. code-block:: bash

        cd path/to/mordicus/tests
        pytest -m "not incompatiblewindows" --cov=../src/Mordicus --cov-report=html:path/to/bluid/html


Simplified use cases
--------------------

Illustrates the functionalities of the library in complete use cases.


.. code-block:: bash

    cd path/to/mordicus
    pytest examples


Doc generation
==============

A sphinx documentation can be generated from the main folder:

.. code-block:: bash

    python setup.py build_sphinx

License and contribution
========================

* `GNU LGPLv3 license <https://gitlab.com/mor_dicus/mordicus/-/blob/master/LICENSE>`_
* `Contribution guide <https://gitlab.com/mor_dicus/mordicus/-/blob/master/CONTRIBUTING.md>`_
