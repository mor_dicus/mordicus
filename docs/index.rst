================
Mordicus library
================


The open-source library 'Mordicus' has been developped by the consortium of the FUI project
MOR_DICUS, see :numref:`consortium`.

.. figure:: _static/consortium.jpg
   :name: consortium
   :align: center
   :width: 45%

   Members of the MOR_DICUS consortium.


The code is hosted on Gitlab_.

.. _Gitlab: https://gitlab.com/mor_dicus/mordicus


The library Mordicus mainly contains a datamodel adapted to Reduced-Order Modeling,
and serves as a core module for other reduced-order modeling libraries, called applicative modules,
developed by the members of the MOR_DICUS consortium:

* **Safran**: genericROM (GitlabGenericROM_, DocumentationGenericROM_)

.. _GitlabGenericROM: https://gitlab.com/drti/genericROM
.. _DocumentationGenericROM: https://genericROM.readthedocs.io/en/latest/




.. toctree::
   :maxdepth: 2
   :caption: Introduction

   intro

.. toctree::
   :maxdepth: 2
   :caption: Methodology

   _methods/index

.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   _tutorials/index


.. toctree::
   :maxdepth: 2
   :caption: Autodoc

   _source/Mordicus



.. toctree::
   :caption: Appendix

   genindex



================
Acknowledgements
================

The consortium MOR_DICUS acknowledges financing from BPI France, Systematic and EnergiVie,
see :numref:`acknow`.

.. figure:: _static/acknow.jpg
   :name: acknow
   :align: center
   :width: 45%

   Acknowledgements