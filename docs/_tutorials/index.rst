====================
Documented use cases
====================


.. toctree::
   :maxdepth: 2
   :caption: DataCompression tutorials

   DataCompression/index


.. toctree::
   :maxdepth: 2
   :caption: ParametrizedVariability tutorials

   ParametrizedVariability/index