#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys
import configparser

sys.path.insert(0,os.path.abspath('../src/Mordicus'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.autosummary',
]

templates_path = ['_templates']
autosummary_generate = False
source_suffix = '.rst'
master_doc = 'index'

cfg = configparser.ConfigParser()
cfg.read('../setup.cfg')

project = cfg['metadata']['name']
copyright = cfg['metadata']['copyright']
copyright_holder = cfg['metadata']['copyright_holder']
license = cfg['metadata']['license']
version = cfg['metadata']['version']
release = cfg['metadata']['version']
author  = cfg['metadata']['copyright']

language = 'en'
numfig = True

pygments_style = 'sphinx'
todo_include_todos = False

html_theme = 'sphinx_rtd_theme'
html_title = 'Mordicus'

html_static_path = ['_static']
html_css_files = ['custom.css']
html_logo = "_static/logo.png"
html_theme_options = {
    'logo_only': False,
    'navigation_depth': 2,
}

htmlhelp_basename = 'MordicusDoc'
add_module_names = False


examplesFolders = [
    'DataCompression/SnapshotsPODCompressionMeca',
    'DataCompression/SnapshotsPODCompressionThermal',
    'ParametrizedVariability/Regression/RegressionThermal',
    ]


def make_zipfile(output_filename, source_dir):
    import shutil
    shutil.make_archive(output_filename, 'zip', source_dir)


def run_apidoc(_):
    import sphinx.ext.apidoc
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    target_dir = os.path.join(cur_dir, '_source')
    module = os.path.join(cur_dir, '../src/Mordicus')
    template_dir = os.path.join(cur_dir, templates_path[0])

    # if needed, uncomment the next line
    # make_zipfile(os.path.join(cur_dir, '_tutorials/TestsData), os.path.join(cur_dir, '../tests/TestsData'))

    for folder in examplesFolders:
        make_zipfile(os.path.join(cur_dir, '_tutorials/'+folder+'/example'+folder.split('/')[-1]), os.path.join(cur_dir, '../examples/'+folder))

    sphinx.ext.apidoc.main(['-T', '-M', '-e', '-f', '-t', template_dir, '-o', target_dir, module])


def setup(app):
    app.connect('builder-inited', run_apidoc)

